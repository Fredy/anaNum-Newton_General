unit mFuncImp;

{$mode objfpc}{$H+}

interface

uses
   Classes, SysUtils, math, mValue;

// Básicas : +, -, *, /, ^, negación.
function fsum(const args : array of TValue) : TValue;
function fsub(const args : array of TValue) : TValue;
function fmul(const args : array of TValue) : TValue;
function fdiv(const args : array of TValue) : TValue;
function fpow(const args : array of TValue) : TValue;
function fneg(const args : array of TValue) : TValue;

// ln, exp, log(base, número)
function fln(const args : array of TValue) : TValue;
function fexp(const args : array of TValue) : TValue;
function flog(const args : array of TValue) : TValue;

// trigonométricas sin, cos, tan, cot, sec, csc
function fsin(const args : array of TValue) : TValue;
function fcos(const args : array of TValue) : TValue;
function ftan(const args : array of TValue) : TValue;
function fctg(const args : array of TValue) : TValue;
function fsec(const args : array of TValue) : TValue;
function fcsc(const args : array of TValue) : TValue;

// trigonométricas inversas: arcoseno, arcocoseno, arcotangente
function fasin(const args : array of TValue) : TValue;
function facos(const args : array of TValue) : TValue;
function fatan(const args : array of TValue) : TValue;

// funciones hiperbólicas: sinh, cosh, tanh
function fsinh(const args : array of TValue) : TValue;
function fcosh(const args : array of TValue) : TValue;
function ftanh(const args : array of TValue) : TValue;

// inversas de funciones hiperbólicas: asinh, acosh, atanh
function fasinh(const args : array of TValue) : TValue;
function facosh(const args : array of TValue) : TValue;
function fatanh(const args : array of TValue) : TValue;

function ffact(const args : array of TValue) : TValue;
function fabs(const args : array of TValue) : TValue;
function ffloor(const args : array of TValue) : TValue;
function fceil(const args : array of TValue) : TValue;
function fsqrt(const args : array of TValue) : TValue;

//funciones solo aplicables a matrices
function ftranspose(const args : array of TValue) : TValue;
function finverse(const args : array of TValue) : TValue;
function fdet(const args : array of TValue) : TValue;

//Función especial para construir matrices:
function mbuildMat(const args : array of TValue) : TValue;

implementation


function fsum(const args : array of TValue) : TValue;
var
   mat1, mat2, matTmp : TMatrix;
   i, j, rows, cols : integer;
begin
   mat1:=args[0].getVal();
   mat2:=args[1].getVal();

   if ((args[0].getType() = nMatrix) or (args[1].getType()= nMatrix)) then
   begin
      rows:= min( args[0].rows(),args[1].rows());
      cols:=min(args[0].cols(),args[1].cols());
      SetLength(matTmp,rows,cols);
      for i := 0 to rows - 1 do
      begin
         for j := 0 to cols - 1 do
         begin
            matTmp[i][j]:= mat1[i][j]+mat2[i][j];
         end
      end;
      Result:=TValue.create(matTmp);
   end
   else
   begin
      Result := TValue.create(mat1[0][0] + mat2[0][0]) ;
   end;
end;
function fsub(const args : array of TValue) : TValue;
var
   mat1, mat2, matTmp : TMatrix;
   i, j, rows, cols : integer;
begin
   mat1 := args[0].getVal();
   mat2 := args[1].getVal();

   if ((args[0].getType() = nMatrix) or (args[1].getType() = nMatrix)) then
   begin
      rows := min( args[0].rows(),args[1].rows());
      cols := min(args[0].cols(),args[1].cols());
      SetLength(matTmp,rows,cols);
      for i := 0 to rows - 1 do
      begin
         for j := 0 to cols - 1 do
         begin
            matTmp[i][j]:= mat1[i][j]-mat2[i][j];
         end
      end;
      Result := TValue.create(matTmp);
   end
   else
   begin
      Result := TValue.create(mat1[0][0] - mat2[0][0]) ;
   end;
end;
function fmul(const args : array of TValue) : TValue;
var
   rows, cols, same : integer;
   i, j, k : integer;
   tmp : real;
   mat1, mat2, res : TMatrix;
   num1, num2 : real;
begin
   if ((args[0].getType() = nMatrix) and (args[1].getType() = nMatrix)) then
   begin
      rows := args[0].rows();
      cols := args[1].cols();
      same := min(args[0].cols(), args[1].rows());// TODO: error si cols != rows
      setLength(res, rows, cols);
      mat1 := args[0].getVal();
      mat2 := args[1].getVal();
      for i := 0 to rows - 1 do
      begin
         for j := 0 to cols - 1 do
         begin
            tmp := 0;
            for k := 0 to same - 1 do
               tmp := tmp + mat1[i][k] * mat2[k][j];
            res[i][j] := tmp;
         end;
      end;
      Result := TValue.create(res)
   end
   else if ((args[0].getType() = nReal) and (args[1].getType() = nReal)) then
   begin
      num1 := args[0].getVal()[0][0];
      num2 := args[1].getVal()[0][0];
      Result := TValue.create(num1 * num2);
   end
   else // Matriz * escalar
   begin
      if (args[0].getType() = nReal) then
      begin
         mat1 := args[1].getVal();
         num1 := args[0].getVal()[0][0];
         rows := args[1].rows();
         cols := args[1].cols();
      end
      else
      begin
         mat1 := args[0].getVal();
         num1 := args[1].getVal()[0][0];
         rows := args[0].rows();
         cols := args[0].cols();
      end;

      for i := 0 to rows - 1 do
      begin
         for j := 0 to cols - 1 do
         begin
            mat1[i][j] := mat1[i][j] * num1;
         end;
      end;
      Result := TValue.create(mat1);

   end;

end;
function fdiv(const args : array of TValue) : TValue;
var
   rows, cols : integer;
   i, j: integer;
   mat1: TMatrix;
   num1, num2: real;
begin
   if ((args[0].getType() = nMatrix) and (args[1].getType() = nReal)) then
   begin
      mat1 := args[0].getVal();
      num1 := args[1].getVal()[0][0];

      rows := args[0].rows();
      cols := args[0].cols();

      for i := 0 to rows - 1 do
      begin
         for j := 0 to cols - 1 do
            mat1[i][j] := mat1[i][j] / num1;
      end;
      Result := TValue.create(mat1);
   end
   else
   begin
      num1 := args[0].getVal()[0][0];
      num2 := args[1].getVal()[0][0];
      Result := TValue.create(num1 / num2);
   end;

end;

function fpow(const args : array of TValue) : TValue;       //Maria/potencia de matrices
var
   base, matTmp : TMatrix;
   i, j, k, p, rows, cols,powerValue : integer;
   tmp :real;
begin
   powerValue:=round(args[1].getVal()[0][0]);
   if ((args[0].getType() = nMatrix) and (args[0].rows() = args[0].cols())) then
   begin
   rows := args[0].rows();
   cols := args[0].cols();
   setLength(matTmp, rows, cols);
   base := args[0].getVal();
   for p:=1 to powerValue-1 do
   begin
     if p = 1 then //a^2
     begin
       for i := 0 to rows - 1 do
       begin
          for j := 0 to cols - 1 do
          begin
             tmp := 0;
             for k := 0 to cols - 1 do
                tmp := tmp + base[i][k] * base[k][j];
             matTmp[i][j] := tmp;
          end;
       end;
     end
     else   //a^n Todo:revisar q funcione bien para este caso
     begin

     for i := 0 to rows - 1 do
       begin
          for j := 0 to cols - 1 do
          begin
             tmp := 0;
             for k := 0 to cols - 1 do
                tmp := tmp + matTmp[i][k] * base[k][j];
             matTmp[i][j] := tmp;
          end;
       end;
     end;
   end;
   Result:=TValue.create(matTmp);
   end
   else
   begin
      Result := TValue.create(power(args[0].getVal()[0][0], powerValue)) ;
   end;
end;
function fneg(const args : array of TValue) : TValue;
var
   matTmp : TMatrix;
   i, j, rows, cols : integer;
begin
   rows:= args[0].rows();
   cols:= args[0].cols();
   SetLength(matTmp,rows,cols);
   for i := 0 to rows - 1 do
   begin
      for j := 0 to cols - 1 do
      begin
         matTmp[i][j]:= (args[0].getVal()[i][j])*(-1);
      end
   end;
   Result:=TValue.create(matTmp);
end;

function fln(const args : array of TValue) : TValue;
begin
   Result := TValue.create(ln(args[0].getVal()[0][0]));
end;

function fexp(const args : array of TValue) : TValue;
begin
   Result := TValue.create( exp(args[0].getVal()[0][0]));
end;

function flog(const args : array of TValue) : TValue;
begin
   Result := TValue.create( logn(args[0].getVal()[0][0],args[1].getVal()[0][0]));
end;

function fsin(const args : array of TValue) : TValue;
begin
   Result := TValue.create(  sin(args[0].getVal()[0][0]));
end;

function fcos(const args : array of TValue) : TValue;
begin
   Result := TValue.create(  cos(args[0].getVal()[0][0]));
end;


function ftan(const args : array of TValue) : TValue;
begin
   Result := TValue.create(  tan(args[0].getVal()[0][0]));
end;

function fctg(const args : array of TValue) : TValue;
begin
   Result := TValue.create(  cotan(args[0].getVal()[0][0]));
end;

function fsec(const args : array of TValue) : TValue;
begin
   Result := TValue.create(  sec(args[0].getVal()[0][0]));
end;

function fcsc(const args : array of TValue) : TValue;
begin
   Result := TValue.create(  cosecant(args[0].getVal()[0][0]));
end;

function fasin(const args : array of TValue) : TValue;
begin
   Result := TValue.create(  arcsin(args[0].getVal()[0][0]));
end;

function facos(const args : array of TValue) : TValue;
begin
   Result := TValue.create(  arccos(args[0].getVal()[0][0]));
end;

function fatan(const args : array of TValue) : TValue;
begin
   Result := TValue.create(  arctan(args[0].getVal()[0][0]));
end;

function fsinh(const args : array of TValue) : TValue;
begin
   Result := TValue.create(  sinh(args[0].getVal()[0][0]));
end;

function fcosh(const args : array of TValue) : TValue;
begin
   Result := TValue.create(  cosh(args[0].getVal()[0][0]));
end;

function ftanh(const args : array of TValue) : TValue;
begin
   Result := TValue.create(  tanh(args[0].getVal()[0][0]));
end;

function fasinh(const args : array of TValue) : TValue;
begin
   Result := TValue.create(  arcsinh(args[0].getVal()[0][0]));
end;

function facosh(const args : array of TValue) : TValue;
begin
   Result := TValue.create(  arccosh(args[0].getVal()[0][0]));
end;

function fatanh(const args : array of TValue) : TValue;
begin
   Result := TValue.create(  arctanh(args[0].getVal()[0][0]));
end;

function ffact(const args : array of TValue) : TValue;
var
   i : integer;
   res : real;
begin
   res := 1;
   for i := 1 to floor(args[0].getVal()[0][0]) do
      res := res * i;
   Result := TValue.create(res);
end;

function fabs(const args : array of TValue) : TValue;
begin
   Result := TValue.create(  abs(args[0].getVal()[0][0]));
end;

function ffloor(const args : array of TValue) : TValue;
begin
   Result := TValue.create(  floor(args[0].getVal()[0][0]));
end;

function fceil(const args : array of TValue) : TValue;
begin
   Result := TValue.create(  ceil(args[0].getVal()[0][0]));
end;

function fsqrt(const args : array of TValue) : TValue;
begin
   Result := TValue.create(  sqrt(args[0].getVal()[0][0]));
end;

function ftranspose(const args : array of TValue) : TValue;
var
   transposeMat : TMatrix;
   i, j, rows, cols : integer;
begin
   rows:= args[0].rows();
   cols:= args[0].cols();
   SetLength(transposeMat,cols,rows);
   for i := 0 to rows - 1 do
   begin
      for j := 0 to cols - 1 do
      begin
         transposeMat[j][i]:= (args[0].getVal()[i][j]);
      end
   end;
   Result:=TValue.create(transposeMat);
end;
function finverse(const args : array of TValue) : TValue;
var
   inverseMat, detAdjMat, mat1 : TMatrix;
   setAdjMatrix : array of array of TMatrix;
   i, a, b, j, k, l, rows, cols  : integer;
   det :real;
   transposed , rtransposed, tDet:TValue;
begin
   rows:= args[0].rows();
   cols:= args[0].cols();
   mat1:= args[0].getVal();
   SetLength(inverseMat,cols,rows);
   if rows = cols then
   begin
        if rows = 2 then
        begin
          det :=  fdet(TValue.create(mat1)).getVal()[0][0];
          if det = 0 then exit;
          inverseMat[0][0] := (mat1[1][1] / det);
          inverseMat[0][1] := (-1*(mat1[0][1]) / det);
          inverseMat[1][0] := (-1*(mat1[1][0]) / det);
          inverseMat[1][1] := (mat1[0][0] / det);
        end
        else
        begin
          SetLength(setAdjMatrix,rows,cols,rows-1,cols-1);
          for a:=0 to rows-1 do              //set matriz adjunta
          begin
            for b:=0 to rows-1 do
            begin
              k:=0;
              l:=0;
              for i:=0 to rows-1 do
              begin
                for j:=0 to rows-1 do
                begin
                  if (i <> a) and (j<>b) then
                  begin
                  setAdjMatrix[a][b][k][l]:=mat1[i][j];
                  l := l+1;
                   if l = cols-1 then
                   begin
                        k := k+1;
                        l := l mod (cols-1);
                   end;
                  end;
                end;
              end;
            end;
          end;

          SetLength(detAdjMat,rows,cols);
          for a:=0 to rows-1 do  //set determinantes matriz adjunta
          begin
            for b:=0 to rows-1 do
            begin
              detAdjMat[a][b] := (fdet(TValue.create(setAdjMatrix[a][b])).getVal()[0][0])*power(-1,a+b);
            end;
          end;

          transposed := TValue.create(detAdjMat);
          rtransposed := ftranspose(transposed);
          det := (fdet(args[0])).getVal()[0][0];
          if det = 0 then exit; // TODO :si el det = 0 no existe la inversa
          det := 1/det;
          tDet := TValue.create(det);
          inverseMat:=(fmul([tDet,rtransposed])).getVal();
        end;
        Result:=TValue.create(inverseMat);
   end;
end;

function fdet(const args : array of TValue) : TValue;
var
   i, j, k, a, b, cols : integer;
   det : real;
   mat1, auxMat :TMatrix;
begin
   mat1 := args[0].getVal();
   if args[0].cols()=2 then
   begin
        det := (mat1[0][0]*mat1[1][1])-(mat1[0][1]*mat1[1][0]);
        Result:= TValue.create(det);
        exit;
   end;

   det := 0;
   cols := args[0].cols();
   for i:=0 to cols -1 do
   begin
     SetLength(auxMat,cols-1,cols-1);
     j:= 0;
     k:= 0;
     for a:=0 to cols-1 do
     begin
       for b:=0 to cols-1 do
       begin
            if (a<>0) and (b<>i) then
            begin
                 auxMat[j][k] := mat1[a][b];
                 k := k+1;
                 if k = cols-1 then
                 begin
                      j := j+1;
                      k := k mod (cols-1);
                 end;
            end;
       end;
     end;
     det := det + ((mat1[0][i]*(fdet(TValue.create(auxMat))).getVal()[0][0])*power(-1,i));
   end;
   Result:=TValue.create(det);
end;

function mbuildMat(const args : array of TValue) : TValue;
var
   n, cols, rows : integer;
   i : integer;
   auxArr : array of real;
   size : integer;
begin
   size := length(args);
   rows := floor(args[size - 2].getVal()[0][0]);
   cols := floor(args[size - 1].getVal()[0][0]);
   n :=  rows * cols;
   setLength(auxArr, n);
   for i := 0 to n - 1 do
   begin
      auxArr[i] := args[i].getVal()[0][0];
   end;

   Result := TValue.create(rows, cols, auxArr);
end;


end.
