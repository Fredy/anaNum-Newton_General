unit mMaps;

{$mode objfpc}{$H+}

interface
uses
   gmap, gutil, mFunction,mValue;

type
   TStringCompare = specialize TLess<string>;
   TFunctionMap = specialize TMap<string, TFunc, TStringCompare>;
   TVarMap = specialize TMap<string, TValue, TStringCompare>;

   PtrFunctionMap = ^TFunctionMap;
   PtrVarMap = ^TVarMap;
implementation
end.
