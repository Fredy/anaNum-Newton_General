unit mFunction;

{$mode objfpc}{$H+}

interface

uses
   Classes, mValue;

type
   functionType = function(const args : array of TValue) : TValue;

   TFunc = class
   protected
      _numOp : integer;
      _func : functionType;
   public
      constructor create(ffunction : functionType; numOp : integer);

      function numOp : integer;
      function solve(const args : array of TValue) : TValue;
   end;

implementation

constructor TFunc.create(ffunction : functionType; numOp : integer);
begin
   self._func := ffunction;
   self._numOp := numOp;
end;

function TFunc.numOp : integer;
begin
   Result := self._numOp;
end;

function TFunc.solve(const args : array of TValue) : TValue;
begin
   // NOTA: es necesario comprobar que la cantidad de parámetros
   // ingresados sea igual a la de _numOp ?
   // lenght(args) = _numOp;
   Result := self._func(args);
end;

end.
