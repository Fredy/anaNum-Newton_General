
unit NewtonGenMethod;

{$mode objfpc}{$H+}

interface

uses
   math, mCalculator, mFuncImp, mValue;

type
   TArrStr = array of string;

   TNewtonGenMethod = class
   public
      _function : TCalculator;
      _vars : TArrStr;
      _point : TMatrix; // Mat[n][0];
      _nextPoint : TMatrix; // Mat[n][0];
      _numOfVars : integer;
   public
      constructor create();
      destructor destroy(); override;

      procedure setNonLinearSys(sys : string);
      procedure setInitialPoint(point : TMatrix);
      procedure setVars(vars : TArrStr); // Deben ingresarce en el orden deseado : ej: x, y, z ...
      function getPoint() : TMatrix;
      function getNextPoint() : TMatrix;
      function getError() : real;
   end;

implementation

constructor TNewtonGenMethod.create();
begin
   _function := TCalculator.create();
   _numOfVars := 0;
end;

destructor TNewtonGenMethod.destroy();
begin
   _function.destroy();
end;

procedure TNewtonGenMethod.setNonLinearSys(sys : string);
begin
   _function.solveExpression(sys);
end;

procedure TNewtonGenMethod.setInitialPoint(point : TMatrix);
begin
   _point := point;
   _nextPoint := point;
end;

procedure TNewtonGenMethod.setVars(vars : TArrStr);
begin
   _vars := vars;
   _numOfVars := length(_vars);
end;

function TNewtonGenMethod.getNextPoint() : TMatrix;
var
   invJacobian, func, auxMat: TMatrix;
   auxDeriv, h : real;
   valsArr : array of real;
   i, j : integer;
begin
   h := 1e-10;
   _point := _nextPoint;

   setLength(valsArr, _numOfVars);
   setLength(invJacobian, _numOfVars, _numOfVars);

   for j := 0 to _numOfVars - 1 do
      valsArr[j] := _point[j][0];
   func := _function.solveSavedExpression(_vars, valsArr).getVal(); // Mat[n][0];

   // Para la jacobiana:
   for i := 0 to _numOfVars - 1 do
   begin
      // Establezco el valor del punto :
      for j := 0 to _numOfVars - 1 do
         valsArr[j] := _point[j][0];
      // Le sumo h a la variable con la estoy sacando derivada parcial:
      valsArr[i] := valsArr[i] + h;
      // Evalúo en ese punto:
      auxMat := _function.solveSavedExpression(_vars, valsArr).getVal(); // Mat[n][0];
      // Asigno la derivada parcial en la matriz del jacobiano
      for j := 0 to _numOfVars - 1 do
         invJacobian[j][i] := (auxMat[j][0] - func[j][0]) / h;
      // Y repito para cada variable ...
   end;

   // La inversa de la jacobiana:
   invJacobian := finverse([TValue.create(invJacobian)]).getVal();
   // Hallo el siguiente punto:
   _nextPoint := fsub(
      [TValue.create(_point),
       fmul([TValue.create(invJacobian), TValue.create(func)])]
                     ).getVal();

   // TODO: invJacobian pasarlo a jacobian y crear un TValue que se llame invJacobian
   // y en lugar de hacer fsub hacer una resta con un for plz;
   Result := _nextPoint;
end;

function TNewtonGenMethod.getPoint() : TMatrix;
begin
   Result := _point;
end;

function TNewtonGenMethod.getError() : real;
var
   tmp, aux : real;
   i : integer;
begin
   for i := 0 to _numOfVars - 1 do
   begin
      aux := _point[i][0] - _nextPoint[i][0];
      tmp := tmp + power(aux, 2);
   end;
   Result := sqrt(tmp);
end;

end.
