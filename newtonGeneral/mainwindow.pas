unit mainWindow;

{$mode objfpc}{$H+}

interface

uses
   Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Grids,
   StdCtrls, Interfaces,math, mValue , NewtonGenMethod;

type

   { TForm1 }

   TForm1 = class(TForm)
      btnSolve: TButton;
      edtError: TEdit;
      edtVars: TEdit;
      edtFunction: TEdit;
      edtInitPoint: TEdit;
      lblVars: TLabel;
      lblInitPoint: TLabel;
      lblFunction: TLabel;
      lblError: TLabel;
      stgResGrid: TStringGrid;
      procedure btnSolveClick(Sender: TObject);
      procedure FormCreate(Sender: TObject);
      procedure FormDestroy(Sender: TObject);
      procedure stgResGridPrepareCanvas(sender: TObject; aCol, aRow: Integer;
         aState: TGridDrawState);
   private
      method : TNewtonGenMethod;

      function tvalueToStr(input :TValue) : string;
      procedure CleanStringGrid;
   public
      { public declarations }
   end;

var
   Form1: TForm1;
const
   realFormat = '0.#######';
implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
begin
   method := TNewtonGenMethod.create();
end;

procedure TForm1.btnSolveClick(Sender: TObject);
var
   i : integer;
   minError, error : real;
   vars : TArrStr;
   point : TMatrix;
   tmpStrList1, tmpStrList2 : TStringList;
   sizeOfVars : integer;
   tableRow : integer;
begin
   if (edtFunction.GetTextLen = 0) then
   begin
      ShowMessage('Ingrese una función.');
      exit;
   end;
   if (edtVars.GetTextLen = 0) then
   begin
      ShowMessage('Ingrese las variables.');
      exit;
   end;
   if (edtInitPoint.GetTextLen = 0) then
   begin
      ShowMessage('Ingrese un punto inicial.');
      exit;
   end;
   if (edtError.GetTextLen = 0) then
   begin
      ShowMessage('Ingrese un error.');
      exit;
   end;

   tmpStrList1 := TStringList.create;
   tmpStrList2 := TStringList.create;

   extractStrings([','],[],PChar(edtVars.text), tmpStrList1);
   extractStrings([','],[],PChar(edtInitPoint.text), tmpStrList2);

   if (tmpStrList1.count <> tmpStrList2.count) then
   begin
      ShowMessage('ERROR: la cantidad de variables difiere a la cantidad de valores ingresados.');
      exit;
   end;

   sizeOfVars := tmpStrList1.count;
   setLength(vars, sizeOfVars);
   setLength(point, sizeOfVars, 1);

   for i := 0 to sizeOfVars - 1 do
   begin
      vars[i] := trim(tmpStrList1[i]);
      point[i][0] := strToFloat(tmpStrList2[i]);
   end;

   method.setNonLinearSys(edtFunction.text);
   method.setVars(vars);
   method.setInitialPoint(point);

   minError := strToFloat(edtError.text);
   error := infinity;
   tableRow := 1;

   // TODO: !!!! ARREGLAR TODO LO QUE TIENE TValue.create!!!!!
   self.CleanStringGrid;
   while (error > minError) do
   begin
      stgResGrid.insertRowWithValues(tableRow,[]);
      stgResGrid.Cells[0,tableRow] := IntToStr(tableRow - 1);
      stgResGrid.Cells[1,tableRow] := self.tvalueToStr(TValue.create(method.getPoint()));
      stgResGrid.Cells[2,tableRow] := self.tvalueToStr(TValue.create(method.getNextPoint()));
      error := method.getError();
      stgResGrid.Cells[3,tableRow] := FormatFloat(realFormat, error);
      tableRow := tableRow + 1;
   end;
end;


procedure TForm1.FormDestroy(Sender: TObject);
begin
   method.destroy();
end;

procedure TForm1.stgResGridPrepareCanvas(sender: TObject; aCol, aRow: Integer;
   aState: TGridDrawState);
var
   MyTextStyle: TTextStyle;
begin
   MyTextStyle := stgResGrid.Canvas.TextStyle;
   MyTextStyle.SingleLine := false;
   stgResGrid.Canvas.TextStyle := MyTextStyle;
end;

function TForm1.tvalueToStr(input :TValue) : string;
var
   i, j : integer;
   res :string;
   mat : TMatrix;
   cols, rows : integer;
begin
   mat := input.getVal();
   if (input.getType() = nReal) then
      Result := FormatFloat(realFormat, mat[0][0])
   else
   begin
      rows := input.rows();
      cols := input.cols();
      res := sLineBreak;
      for i := 0 to rows - 1 do
      begin
         for j := 0 to cols - 1 do
         begin
            res += FormatFloat(realFormat, mat[i][j]) + '  ';
         end;
         res += sLineBreak;
      end;
      Result:= res;
   end;
end;

procedure TForm1.CleanStringGrid;
var
   I: Integer;
begin
   for I := 0 to  stgResGrid.ColCount - 1 do
     stgResGrid.Cols[I].Clear;
   stgResGrid.RowCount := 1;
end;


end.
